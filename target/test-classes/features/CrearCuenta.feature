@Regresion
Feature: Formulario para Crear cuenta en el sitio web PruebaQaApp
  El usuario debe poder ingresar al formulario los datos requeridos.
  Cada campo del formulario realiza validaciones de obligatoriedad,
  longitud y formato, el sistema debepresentar las validaciones respectivas
  para cada campo a traves de un globo informativo


  @CasoExitoso
  Scenario: Diligenciamiento exitoso del formulario
  Given Ingreso a la funcionalidad del formulario PruebaQaAPP
  When Diligencio Formulario Crear Cuenta
    | Nombre  | Apellido | Correo_electrónico     | Contraseña_nueva |Fecha_de_nacimiento | 
    | Carlos  | Rendon   | carlosrendon@gmail.com | Carlos123        | 1980-03-10         |
  Then Verifico ingreso ok
  
    @CasoFallido1
  Scenario: Diligenciamiento Fallido del formulario Campo nombre vacio
  Given Ingreso a la funcionalidad del formulario PruebaQaAPP
  When Diligencio Formulario Crear Cuenta
    | Nombre  | Apellido | Correo_electrónico     | Contraseña_nueva |Fecha_de_nacimiento | 
    |         | Rendon   | carlosrendon@gmail.com | Carlos123        | 1980-03-10         |
  Then Verifico ingreso fallido
   
       @CasoFallido2
  Scenario: Diligenciamiento Fallido del formulario Campo apellido vacio
  Given Ingreso a la funcionalidad del formulario PruebaQaAPP
  When Diligencio Formulario Crear Cuenta
    | Nombre  | Apellido | Correo_electrónico     | Contraseña_nueva |Fecha_de_nacimiento | 
    | Carlos  |          | carlosrendon@gmail.com | Carlos123        | 1980-03-10         |
  Then Verifico ingreso fallido
  
      @CasoFallido3
  Scenario: Diligenciamiento Fallido del formulario Campo email con formato incorrecto
  Given Ingreso a la funcionalidad del formulario PruebaQaAPP
  When Diligencio Formulario Crear Cuenta
    | Nombre  | Apellido | Correo_electrónico     | Contraseña_nueva |Fecha_de_nacimiento | 
    | Carlos  | Rendon   | carlosrendon           | Carlos123        | 1980-03-10         |
  Then Verifico ingreso fallido
  
      @CasoFallido4
  Scenario: Diligenciamiento Fallido del formulario Campo fecha con formato incorrecto
  Given Ingreso a la funcionalidad del formulario PruebaQaAPP
  When Diligencio Formulario Crear Cuenta
    | Nombre  | Apellido | Correo_electrónico     | Contraseña_nueva |Fecha_de_nacimiento | 
    | Carlos  | Rendon   | carlosrendon@gmail.com | Carlos123        | 1980-03            |
  Then Verifico ingreso fallido
  
