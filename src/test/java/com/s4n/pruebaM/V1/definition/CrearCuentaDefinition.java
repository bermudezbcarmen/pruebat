package com.s4n.pruebaM.V1.definition;


import com.s4n.pruebaM.V1.steps.CrearCuentaSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import cucumber.api.DataTable;
import java.util.List;

public class CrearCuentaDefinition {
	
	@Steps
	CrearCuentaSteps  crearCuentaSteps;
	
	@Given("^Ingreso a la funcionalidad del formulario PruebaQaAPP$")
	public void ingreso_a_la_funcionalidad_del_formulario_PruebaQaAPP() {
		crearCuentaSteps.cargar_URL();
		
	}

	@When("^Diligencio Formulario Crear Cuenta$")
	public void diligencio_Formulario_Crear_Cuenta(DataTable dtDatosForm) {
		List<List<String>> data= dtDatosForm.raw();
		for (int i=1; i<data.size(); i++) {
			crearCuentaSteps.diligenciar_popup_datos_tabla(data, i);
			try {
				Thread.sleep(2000);
				
			}catch(InterruptedException e) {}
		}
	}

	@Then("^Verifico ingreso ok$")
	public void verifico_ingreso_ok() {
		crearCuentaSteps.verifica_Ingreso_Formulario_Exitoso();
	}

	@Then("^Verifico ingreso fallido$")
	public void verifico_ingreso_fallido() {
		crearCuentaSteps.verifica_Ingreso_Formulario_Fallido();
	}

}
