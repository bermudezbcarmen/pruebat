package com.s4n.pruebaM.V1.steps;

import java.util.List;

import com.s4n.pruebaM.V1.pageobject.CrearCuentaPage;

import net.thucydides.core.annotations.Step;

public class CrearCuentaSteps {
	
	CrearCuentaPage crearCuentaPage;
	
	@Step
	public void cargar_URL() {
		crearCuentaPage.open();
	}


	@Step
	public void diligenciar_popup_datos_tabla(List<List<String>> data, int id) {
		crearCuentaPage.nombre(data.get(id).get(0).trim());
		crearCuentaPage.apellido(data.get(id).get(1).trim());
		crearCuentaPage.correo_Electronico(data.get(id).get(2).trim());
		crearCuentaPage.contrasena_Nueva(data.get(id).get(3).trim());
		crearCuentaPage.fechaNacimiento(data.get(id).get(3).trim());
		crearCuentaPage.genero();
		crearCuentaPage.crearCuenta();
		
	}
	
	@Step
	public void verifica_Ingreso_Formulario_Exitoso() {
		crearCuentaPage.verificar_Ingreso_Exitoso();
	}
	
	@Step
	public void verifica_Ingreso_Formulario_Fallido() {
		crearCuentaPage.verificar_Ingreso_Exitoso();
	}
}