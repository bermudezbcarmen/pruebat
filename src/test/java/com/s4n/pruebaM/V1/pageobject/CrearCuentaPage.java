package com.s4n.pruebaM.V1.pageobject;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

//import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.openqa.selenium.Alert;

@DefaultUrl("http://3.87.50.247:3000/#/")

public class CrearCuentaPage extends PageObject {
	
	//Ingresar Nombre
	@FindBy(id="id_nombre")
	public WebElementFacade txtNombre;

	//Ingresar Apellido
	@FindBy (id="id_apellido") 
	public WebElementFacade txtApellido;
	
	//Ingresar Correo Electronico
	@FindBy (id="id_email")
	public WebElementFacade txtCorreoElectronico;
	
	//Ingresar Nueva Contraseña
	@FindBy (id="id_password")
	public WebElementFacade txtNuevaContrasena;
	
	//Ingresar Fecha de nacimiento
	@FindBy (name="birthdate")
	public WebElementFacade txtFechaNacimiento;
	
	//Seleccionar Genero
	@FindBy (id="id_genero_hombre")
	public WebElementFacade genero;
	
	//Crear Cuenta
	@FindBy (xpath="//button[@class='btn btn-default' and @type='submit']")
	public WebElementFacade btnCrearCuenta;
	
	public void nombre(String datoPrueba) {
		txtNombre.click();
		txtNombre.clear();
		txtNombre.sendKeys(datoPrueba);
	}
	
	public void apellido(String datoPrueba) {
		txtApellido.click();
		txtApellido.clear();
		txtApellido.sendKeys(datoPrueba);	
	}
	
	public void correo_Electronico(String datoPrueba) {
		txtCorreoElectronico.click();
		txtCorreoElectronico.clear();
		txtCorreoElectronico.sendKeys(datoPrueba);
	}
	public void contrasena_Nueva(String datoPrueba) {
		txtNuevaContrasena.click();
		txtNuevaContrasena.clear();
		txtNuevaContrasena.sendKeys(datoPrueba);
	}
	public void fechaNacimiento(String datoPrueba) {
		txtFechaNacimiento.click();
		txtFechaNacimiento.clear();
		txtFechaNacimiento.sendKeys(datoPrueba);
	}
	public void genero() {
		genero.click();
	}
	public void crearCuenta() {
		btnCrearCuenta.click();
		try {
			Thread.sleep(2000);
			
		}catch(InterruptedException e) {}
	}

	public void verificar_Ingreso_Exitoso() {
		String text = getDriver().switchTo().alert().getText();
		assertThat(text.isEmpty(), is(false));
		
	}
	public void verificar_Ingreso_Fallido() {
		String text = getDriver().switchTo().alert().getText();
		assertThat(text.isEmpty(), is(true));
		
	}

}
